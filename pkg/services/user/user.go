package user

type User struct {
	ID       int    `json:"id,omitempty"`
	NickName string `json:"nickName,omitempty"`
	Password string `json:"password,omitempty"`
	Status   int    `json:"status,omitempty"`
	Level    int    `json:"level,omitempty"`
}
