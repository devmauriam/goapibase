package user

import (
	"github.com/gorilla/mux"
)

//UserRoutes crea el subRouter en este caso Crea las rutas EndPoint para el crud User
func UserRoutes(router *mux.Router) {
	subRouter := router.PathPrefix("/config").Subrouter()
	subRouter.HandleFunc("/user", Users)
	subRouter.HandleFunc("/user/{ID}", UserByID)
}
