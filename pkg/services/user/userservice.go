package user

import (
	"ApiRestful-Standar/pkg/response"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

//Users direcciona la peticion del cliente deacuerdo al metodo solicitado
func Users(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		{
			code, _, users := GetUsers()
			data, _ := json.Marshal(users)
			response.SendResponse(w, code, data)
			break
		}
	case "POST":
		{
			newuser := User{}
			json.NewDecoder(r.Body).Decode(&newuser)
			code, message := PostUser(newuser)
			response.SendResponse(w, code, []byte(message))
			break
		}
	default:
		{
			response.SendResponse(w, 405, []byte("405 Method Not Allowed"))
		}
	}
}

//UsersByID direcciona la peticion del cliente deacuerdo al metodo solicitado
func UserByID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["ID"])
	switch r.Method {
	case "GET":
		{
			code, _, user := GetUser(id)
			data, _ := json.Marshal(user)
			response.SendResponse(w, code, data)
			break
		}
	case "PUT":
		{
			newuser := User{}
			json.NewDecoder(r.Body).Decode(&newuser)
			newuser.ID = id
			code, message := PutUser(newuser)
			response.SendResponse(w, code, []byte(message))
			break
		}
	case "DELETE":
		{
			code, message := DeleteUser(id)
			response.SendResponse(w, code, []byte(message))
			break
		}
	default:
		{
			response.SendResponse(w, 405, []byte("405 Method Not Allowed"))
		}
	}
}
