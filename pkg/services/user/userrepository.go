package user

import (
	"ApiRestful-Standar/pkg/repository"
	"fmt"
)

//insertar usuario en la base de datos
func PostUser(user User) (int, string) {
	connection := repository.MainConnection()
	defer connection.Close()
	sql := `INSERT INTO USER (ID,NICKNAME,PASSWORD,STATUS,LEVEL) VALUES(NULL,?,?,?,?)`
	_, err := connection.Exec(sql, user.NickName, user.Password, user.Status, user.Level)
	if err != nil {
		return 500, "error" + err.Error()
	}
	return 200, "OK"
}

//consultar todos los usarios de la base de datos
func GetUsers() (int, string, []User) {
	connection := repository.MainConnection()
	defer connection.Close()
	user := User{}
	users := []User{}
	sql := `SELECT * FROM USER`
	row, err := connection.Query(sql)
	if err != nil {
		return 500, "error" + err.Error(), nil
	}
	for row.Next() {
		row.Scan(&user.ID, &user.NickName, &user.Password, &user.Status, &user.Level)
		users = append(users, user)
	}
	return 200, "OK", users
}

//consultar un usuario dado un ID
func GetUser(id int) (int, string, User) {
	connection := repository.MainConnection()
	defer connection.Close()
	user := User{}
	sql := `SELECT * FROM USER WHERE ID=?`
	row, err := connection.Query(sql, id)
	if err != nil {
		fmt.Println(err.Error())
		return 500, "error" + err.Error(), user
	}
	for row.Next() {
		row.Scan(&user.ID, &user.NickName, &user.Password, &user.Status, &user.Level)
	}
	if user.ID == 0 {
		return 404, "user not found", user
	} else {
		return 200, "OK", user
	}
}

//actualizar todos los campos de la base de datos
func PutUser(user User) (int, string) {
	connection := repository.MainConnection()
	defer connection.Close()
	sql := `UPDATE USER SET NICKNAME=?, PASSWORD=?, STATUS=?, LEVEL=? WHERE ID=?`
	fmt.Println(user)
	_, err := connection.Exec(sql, user.NickName, user.Password, user.Status, user.Level, user.ID)
	if err != nil {
		fmt.Println(err.Error())
		return 500, "error" + err.Error()
	}
	return 200, "OK"
}

//borrar un usuario
func DeleteUser(id int) (int, string) {
	connection := repository.MainConnection()
	defer connection.Close()
	sql := `DELETE FROM USER WHERE ID=?`
	_, err := connection.Exec(sql, id)
	if err != nil {
		return 500, "error" + err.Error()
	}
	return 200, "OK"
}
