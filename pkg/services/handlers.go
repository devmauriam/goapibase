package service

import (
	"ApiRestful-Standar/pkg/services/user"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

//GetRouter funcion encargada de retornar todas las rutas del api restful
func GetRoutes() http.Handler {
	mainRoutes := mux.NewRouter()
	mainRoutes.HandleFunc("/", home).Methods("GET")
	user.UserRoutes(mainRoutes)

	return mainRoutes
}

//Home() handlerFunc de la ruta "/" tendra la informacion de la api
func home(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Api Gateway")
	fmt.Fprintln(w, "IP Server:", r.Host)
	fmt.Fprintln(w, "IP Cliente:", r.RemoteAddr)
	fmt.Fprintln(w, "Method:", r.Method)
}
