package repository

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

//constantes de los directorios de las base de datos y sus respectivos nombres
const (
	dir              = "../pkg/database/"
	nameMainDataBase = "DataBaseMain.db"
	nameLogDataBase  = "DataBaseLog.db"
)

//funcion que permite conectar a la base de datos principal
//base de datos contendra los usuario y sus perfiles
//retorna la conexion
func MainConnection() (db *sql.DB) {
	connection, err := sql.Open("sqlite3", dir+nameMainDataBase)
	if err != nil {
		fmt.Println("error connecting to database")
	}
	return connection
}

//funcion que permite conetar a la base de datos Log
//base de datos contendra los registros de actividades de los usuario y del gateway
//retorna la conexion
func LogConnection() (db *sql.DB) {
	connection, err := sql.Open("sqlite3", dir+nameLogDataBase)
	if err != nil {
		fmt.Println("error connecting to database")
	}
	return connection
}
