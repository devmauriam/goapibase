package server

import (
	"ApiRestful-Standar/config"
	service "ApiRestful-Standar/pkg/services"

	"fmt"
	"log"
	"net/http"
)

func StartServer() {

	conf, err := config.GetConfig()
	if err != nil {
		log.Fatalln("error load settings ")
	}
	fmt.Println("server online Port:", conf.Port)
	http.ListenAndServe(":"+conf.Port, service.GetRoutes())

}
