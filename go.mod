module ApiRestful-Standar

go 1.18

require (
	github.com/gorilla/mux v1.8.0
	github.com/mattn/go-sqlite3 v1.14.15
	gopkg.in/yaml.v2 v2.4.0
)
